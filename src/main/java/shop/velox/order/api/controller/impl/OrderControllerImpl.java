package shop.velox.order.api.controller.impl;

import static org.apache.commons.lang3.StringUtils.isBlank;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.order.api.controller.OrderController;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;
import shop.velox.order.service.OrderService;

@RestController
@Slf4j
@RequiredArgsConstructor
public class OrderControllerImpl implements OrderController {

  private final OrderService orderService;

  @Override
  public ResponseEntity<OrderDto> createOrder(final CreateOrderDto createOrderDto) {
    var createdOrderDto = orderService.createOrder(createOrderDto);
    log.info("created order dto {}:", createdOrderDto);

    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(createdOrderDto);
  }

  @Override
  public ResponseEntity<OrderDto> getOrder(final String orderId) {
    log.info("getOrder: {}", orderId);

    OrderDto orderDto = orderService.getOrder(orderId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    return ResponseEntity.ok(orderDto);
  }


  @Override
  public Page<OrderDto> getOrders(final String userId, Pageable pageable,
      final OrderStatus filter, String cartCode) {
    if (isBlank(userId)) {
      log.info("get all Orders as admin with orderStatus: {}, cartCode: {} and pageable: {}",
          filter, cartCode, pageable);
      return orderService.getAll(pageable, filter, cartCode);
    } else {
      log.info("get user Orders as user {} with filter: {}, cartCode: {} and pageable: {}", userId,
          filter, cartCode, pageable);
      return orderService.getAllByUserId(userId, pageable, filter, cartCode);
    }
  }

  @Override
  public ResponseEntity<OrderDto> update(String orderId, UpdateOrderDto order) {
    log.info("update {}, {}", orderId, order);

    OrderDto updatedOrderDto = orderService.updateOrder(orderId, order);
    return ResponseEntity.ok(updatedOrderDto);
  }

  @Override
  public ResponseEntity<OrderDto> updateStatus(String orderId, OrderStatus orderStatus,
      String orderHash) {
    log.info("update {}, {}", orderId, orderStatus);

    OrderDto updatedOrderDto = orderService.updateStatus(orderId, orderStatus, orderHash);

    return ResponseEntity.ok(updatedOrderDto);
  }

}
