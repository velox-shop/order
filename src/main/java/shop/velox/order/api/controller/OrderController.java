package shop.velox.order.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;

@Tag(name = "Order", description = "the Order API")
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public interface OrderController {

  String ORDER_STATUS_FILTER = "statusFilter";

  @Operation(summary = "Creates an Order", description = "")
  @PostMapping
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Order created.")
  })
  ResponseEntity<OrderDto> createOrder(
      @Parameter(description = "Parameters for Order.", required = true)
      @RequestBody CreateOrderDto createOrderDto
  );

  @Operation(summary = "Retrieves an Order", description = "Retrieves an Order")
  @GetMapping(value = "/{id}")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "Order is found"),
      @ApiResponse(responseCode = "404",
          description = "Order not found",
          content = @Content(schema = @Schema()))
  })
  ResponseEntity<OrderDto> getOrder(
      @Parameter(description = "Id of the Order. Cannot be empty.", required = true)
      @PathVariable("id") String orderId);


  @Operation(summary = "gets all Orders", description = "Paginated. Not available to guests, only to order owner or Order Admin")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation")
  })
  @GetMapping
  Page<OrderDto> getOrders(
      @Parameter(description = "Id of the user that requires orders")
      @RequestParam(name = "userId", required = false, defaultValue = "") String userId,

      @PageableDefault(size = 3) @SortDefault.SortDefaults({
          @SortDefault(sort = "createdDateTime", direction = Sort.Direction.DESC)
      }) Pageable pageable,

      @Parameter(description = "Return orders with different status (DRAFT, ORDERED). If statusFilter is empty, return all orders. Can be empty.")
      @RequestParam(name = ORDER_STATUS_FILTER, required = false) final OrderStatus filter,

      @Parameter(description = "Code of the Cart")
      @RequestParam(name = "cartCode", required = false) String cartCode
  );


  @Operation(summary = "Updates an Order", description = "Updates an Order")
  @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Order Updated"),
      @ApiResponse(responseCode = "404", description = "Order not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "412",
          description = "Order was changed after your last request. Your version is stale",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422",
          description = "You tried to set read-only parameters (e.g. a price)",
          content = @Content(schema = @Schema()))
  })
  ResponseEntity<OrderDto> update(
      @Parameter(description = "Id of Order. Cannot be empty.", required = true)
      @PathVariable("id") String orderId,

      @Parameter(description = "Parameters for Order.", required = true)
      @RequestBody UpdateOrderDto order);


  @Operation(summary = "Updates an Order Status", description = "Updates an Order Status")
  @PatchMapping(value = "/{id}/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Order Updated"),
      @ApiResponse(responseCode = "404", description = "Order not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "412",
          description = "Order was changed after your last request. Your version is stale",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422",
          description = "You tried to set read-only parameters (e.g. a price)",
          content = @Content(schema = @Schema()))
  })
  ResponseEntity<OrderDto> updateStatus(
      @Parameter(description = "Id of Order. Cannot be empty.", required = true)
      @PathVariable("id") String orderId,

      @Parameter(description = "new order status", required = true)
      @RequestParam OrderStatus orderStatus,

      @Parameter(description = "Hash of the Order. Cannot be empty.", required = true)
      @RequestParam String orderHash);
}
