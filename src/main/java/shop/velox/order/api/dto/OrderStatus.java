package shop.velox.order.api.dto;

public enum OrderStatus {
  DRAFT,
  ORDERED,
  EXPORTED,
}
