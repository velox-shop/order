package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "Charge")
public class ChargeDto {

  @Schema(description = "Type of charge", example = "shipping")
  String type;

  @Schema(description = "Charged Amount", example = "4.99")
  BigDecimal amount;
}
