package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "Order")
public class OrderDto {

  @Schema(description = "Unique identifier of the Order.", example = "507f191e810c19729de860ea")
  private String id;

  @Schema(description = "Unique identifier of the user.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private String userId;

  @Schema(description = "Human-readable identifier for the Order", example = "9de860ea")
  private String code;

  @Schema(description = "When the Order was placed")
  private LocalDateTime placedDateTime;

  @Schema(description = "When the Order was created")
  private LocalDateTime createdDateTime;

  @Schema(description = "When the Order was last modified")
  private LocalDateTime lastModifiedDateTime;

  @Schema(description = "List of items that user wants to buy")
  private List<OrderEntryDto> entries;

  @Schema(description = "Shipping Address")
  private AddressDto shippingAddress;

  @Schema(description = "Shipping Address")
  private AddressDto billingAddress;

  @Schema(description = "charges")
  private List<ChargeDto> charges;

  @Schema(description = "Total price that user will pay")
  private BigDecimal totalPrice;

  @Schema(description = "Currency of the order", example = "CHF")
  private String currencyIsoCode;

  @Schema(description = "Code of the Cart")
  private String cartCode;

  @Schema(description = "Order status")
  private OrderStatus orderStatus;

  @Schema(description = "Payment status")
  private PaymentStatus paymentStatus;

  @Schema(description = "Shipment status")
  private ShipmentStatus shipmentStatus;

  @Schema(description = "Metadata Entries")
  List<MetadataEntryDto> metadata;

  @EqualsAndHashCode.Exclude
  @Schema(description = "Hash of the Order")
  private String hash;

}
