package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder(toBuilder = true)
@Jacksonized
@FieldNameConstants
@Schema(name = "UpdateOrder")
public class UpdateOrderDto {

  @Schema(description = "Human-readable identifier for the Order", example = "9de860ea")
  String code;

  @Schema(description = "List of items that user wants to buy")
  List<OrderEntryDto> entries;

  @Schema(description = "Shipping Address")
  AddressDto shippingAddress;

  @Schema(description = "Shipping Address")
  AddressDto billingAddress;

  @Schema(description = "charges")
  List<ChargeDto> charges;

  @Schema(description = "Total price that user will pay")
  BigDecimal totalPrice;

  @Schema(description = "Currency of the order", example = "CHF")
  String currencyIsoCode;

  @NotNull
  @NotBlank
  @Schema(description = "Code of the Cart")
  String cartCode;

  @NotNull
  @Schema(description = "Order status")
  OrderStatus orderStatus;

  @Schema(description = "Payment status")
  PaymentStatus paymentStatus;

  @Schema(description = "Shipment status")
  ShipmentStatus shipmentStatus;

  @Schema(description = "Metadata Entries")
  List<MetadataEntryDto> metadata;

  @NotNull
  @NotBlank
  @Schema(description = "Hash of the previously loaded Order")
  String hash;

  @Schema(description = "Date and time when the order was placed")
  LocalDateTime placedDateTime;

}
