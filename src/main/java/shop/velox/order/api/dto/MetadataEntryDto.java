package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "MetadataEntry")
public class MetadataEntryDto {

  private String key;

  private List<MetadataValueDto> values;

}
