package shop.velox.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.availability.api.dto.AvailabilityDto;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "OrderEntry")
public class OrderEntryDto {

  @Schema(description = "Id of the Article")
  String articleId;

  @Schema(description = "Name of the Article")
  String articleName;

  @Schema(description = "Availability")
  AvailabilityDto availability;

  @Schema(description = "ordered Quantity")
  BigDecimal quantity;

  @Schema(description = "Unit Price")
  BigDecimal unitPrice;

  @Schema(description = "Price")
  BigDecimal price;

  @Schema(description = "Metadata Entries")
  List<MetadataEntryDto> metadata;

}
