package shop.velox.order.api.client;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;

import java.net.URI;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;

@Slf4j
public class OrderApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public OrderApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/order/v1/orders";
  }

  public ResponseEntity<OrderDto> createOrder(CreateOrderDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateOrderDto> httpEntity = new HttpEntity<>(payload, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .build()
        .toUri();

    log.debug("createOrder with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, OrderDto.class);
  }

  public ResponseEntity<OrderDto> getOrder(String orderId) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateOrderDto> httpEntity = new HttpEntity<>(null, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(orderId)
        .build()
        .toUri();

    log.debug("getOrder with URI: {}", uri);

    return restTemplate.exchange(uri, GET, httpEntity, OrderDto.class);
  }

  public ResponseEntity<OrderDto> updateOrder(String orderId, UpdateOrderDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateOrderDto> httpEntity = new HttpEntity<>(payload, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(orderId)
        .build()
        .toUri();

    log.debug("updateOrder with URI: {}", uri);

    return restTemplate.exchange(uri, PATCH, httpEntity, OrderDto.class);
  }

  public ResponseEntity<OrderDto> updateOrderStatus(String orderId, OrderStatus newOrderStatus,
      String orderHash) {

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(orderId)
        .pathSegment("status")
        .queryParam("orderStatus", newOrderStatus)
        .queryParam("orderHash", orderHash)
        .build()
        .toUri();

    log.debug("updateOrderStatus with URI: {}", uri);

    return restTemplate.exchange(uri, PATCH, null, OrderDto.class);
  }


  public ResponseEntity<? extends Page<OrderDto>> getAllOrders(Pageable pageable,
      @Nullable String userId, @Nullable OrderStatus orderStatus, @Nullable String cartCode) {
    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateOrderDto> httpEntity = new HttpEntity<>(null, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParamIfPresent("page", Optional.ofNullable(pageable).map(Pageable::getPageNumber))
        .queryParamIfPresent("size", Optional.ofNullable(pageable).map(Pageable::getPageSize))
        .queryParamIfPresent("sort", Optional.ofNullable(pageable).map(Pageable::getSort))
        .queryParamIfPresent("userId", Optional.ofNullable(userId))
        .queryParamIfPresent("statusFilter", Optional.ofNullable(orderStatus))
        .queryParamIfPresent("cartCode", Optional.ofNullable(cartCode))
        .build()
        .toUri();

    log.debug("getAllOrders with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<OrderDto>> responseType = new ParameterizedTypeReference<>() {
    };

    return restTemplate.exchange(uri, GET, httpEntity, responseType);
  }

  public ResponseEntity<? extends Page<OrderDto>> getAllOrdersFromUser(String userId,
      Pageable pageable, @Nullable OrderStatus orderStatus, @Nullable String cartCode) {
    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateOrderDto> httpEntity = new HttpEntity<>(null, headers);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParam("userId", userId)
        .queryParamIfPresent("page", Optional.ofNullable(pageable).map(Pageable::getPageNumber))
        .queryParamIfPresent("size", Optional.ofNullable(pageable).map(Pageable::getPageSize))
        .queryParamIfPresent("sort", Optional.ofNullable(pageable).map(Pageable::getSort))
        .queryParamIfPresent("statusFilter", Optional.ofNullable(orderStatus))
        .queryParamIfPresent("cartCode", Optional.ofNullable(cartCode))
        .build()
        .toUri();

    log.debug("getAllOrdersFromUser with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<OrderDto>> responseType = new ParameterizedTypeReference<>() {
    };

    return restTemplate.exchange(uri, GET, httpEntity, responseType);
  }
}
