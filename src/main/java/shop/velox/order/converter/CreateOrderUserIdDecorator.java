package shop.velox.order.converter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import shop.velox.commons.security.service.SecurityService;
import shop.velox.order.api.dto.CreateOrderDto;

@Component
@RequiredArgsConstructor
@Slf4j
public class CreateOrderUserIdDecorator {

  private final SecurityService securityService;

  public String getUserId(CreateOrderDto createOrderDto) {
    String result = securityService.getUserId().orElse(null);
    log.debug("getUserId returns: {}", result);
    return result;
  }

}
