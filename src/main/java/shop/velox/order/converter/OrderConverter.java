package shop.velox.order.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.model.OrderEntity;

@Mapper(uses = {
    MetaDataConverter.class,
    LocalDateTimeConverter.class,
})
public abstract class OrderConverter {

  @Autowired
  protected OrderHashDecorator orderHashDecorator;

  @Mappings({
      @Mapping(target = "hash", ignore = true),
  })
  public abstract OrderDto convertEntityToDto(OrderEntity orderEntity);

  @AfterMapping
  protected void setHash(@MappingTarget OrderDto.OrderDtoBuilder target) {
    String hash = orderHashDecorator.getOrderHash(target.build());
    target.hash(hash);
  }
}
