package shop.velox.order.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.order.api.dto.UpdateOrderDto;
import shop.velox.order.model.OrderEntity;

@Mapper
public interface UpdateOrderPopulator {

  @Mappings({
      @Mapping(target = "id", ignore = true),
      @Mapping(target = "userId", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
      @Mapping(target = "version", ignore = true),
  })
  void update(UpdateOrderDto updateOrderDto, @MappingTarget OrderEntity orderEntity);

}
