package shop.velox.order.converter;


import java.time.LocalDateTime;
import java.util.Optional;
import org.mapstruct.Mapper;

@Mapper
public class LocalDateTimeConverter {

  // Due to MongoDB limitation, we need to remove the nanoseconds from the LocalDateTime
  LocalDateTime convert(LocalDateTime localDateTime) {
    return Optional.ofNullable(localDateTime)
        .map(time -> time.withNano(0))
        .orElse(null);
  }

}
