package shop.velox.order.converter;

import org.springframework.stereotype.Component;
import shop.velox.order.api.dto.OrderDto;

@Component
public class OrderHashDecorator {

  public String getOrderHash(OrderDto orderDto) {
    return Integer.toString(orderDto.hashCode());
  }

}
