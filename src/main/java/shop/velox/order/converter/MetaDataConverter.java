package shop.velox.order.converter;

import org.mapstruct.Mapper;
import shop.velox.order.api.dto.MetadataEntryDto;
import shop.velox.order.api.dto.MetadataValueDto;
import shop.velox.order.model.MetadataEntryEntity;
import shop.velox.order.model.MetadataValueEntity;

@Mapper
public interface MetaDataConverter {

  MetadataEntryDto convert(MetadataEntryEntity metadataEntryEntity);

  MetadataEntryEntity convert(MetadataEntryDto metadataEntryDto);

  MetadataValueDto convert(MetadataValueEntity metadataValueEntity);

  MetadataValueEntity convert(MetadataValueDto metadataValueDto);

}
