package shop.velox.order.converter;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.model.OrderEntity;

@Mapper(uses = {
    CreateOrderUserIdDecorator.class,
    MetaDataConverter.class,
})
public interface CreateOrderConverter {

  @Mappings({
      @Mapping(target = "userId", source = "."),
      @Mapping(target = "id", ignore = true),
      @Mapping(target = "createdDateTime", ignore = true),
      @Mapping(target = "lastModifiedDateTime", ignore = true),
      @Mapping(target = "version", ignore = true),
  })
  OrderEntity convert(CreateOrderDto createOrderDto);

}
