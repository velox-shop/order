package shop.velox.order.permissions;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static shop.velox.commons.security.VeloxSecurityConstants.Authorities.GLOBAL_ADMIN_AUTHORIZATION;
import static shop.velox.order.service.OrderServiceConstants.Authorities.ORDER_ADMIN;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerErrorException;
import shop.velox.commons.security.service.AuthorizationEvaluator;
import shop.velox.commons.security.service.SecurityService;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.service.OrderService;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrderAuthorizationEvaluator {

  private final SecurityService securityService;

  private final AuthorizationEvaluator authorizationEvaluator;

  private final OrderService orderService;

  public boolean canAccessAnyOrder(Authentication authentication) {
    boolean result = securityService.hasAnyAuthority(authentication, List.of(GLOBAL_ADMIN_AUTHORIZATION, ORDER_ADMIN));
    log.debug("Checking if user with authentication: {} can access any cart returns: {}",
        authentication, result);
    return result;
  }

  public boolean isOwnOrder(Authentication authentication, OrderDto orderDto) {
    if(isBlank(orderDto.getUserId())) {
      throw new ServerErrorException("On Order: " + orderDto.getId() + "User id is blank", null);
    }

    var isOwnOrder = authorizationEvaluator.isCurrentUserId(authentication, orderDto.getUserId());
    log.debug("Checking if user with authentication: {} is owner of order: {} returns: {}",
        authentication, orderDto.getId(), isOwnOrder);
    return isOwnOrder;
  }

  public boolean canAccessOrder(Authentication authentication, Optional<OrderDto> orderOptional) {
    if(orderOptional.isEmpty()) {
      // if there is no order, we want 404, not 403
      return true;
    }

    OrderDto orderDto = orderOptional.get();
    if(isBlank(orderDto.getUserId())) {
      throw new ServerErrorException("On Order: " + orderDto.getId() + "User id is blank", null);
    }

    if(canAccessAnyOrder(authentication)) {
      return true;
    }

    return isOwnOrder(authentication, orderDto);

  }

  public boolean canAccessOrderWithId(Authentication authentication, String orderId) {
    boolean result = canAccessAnyOrder(authentication) || canAccessOrder(authentication,
        orderService.getOrder(orderId));
    log.debug("Checking if user with authentication: {} can access order with id: {} returns: {}",
        authentication, orderId, result);
    return result;
  }

  public boolean canAccessAllUserOrders(Authentication authentication, String userId) {
    boolean result = authorizationEvaluator.isCurrentUserId(authentication, userId) || canAccessAnyOrder(authentication);
    log.debug("Checking if user with authentication: {} can access all orders of user: {} returns: {}",
        authentication, userId, result);
    return result;
  }


}
