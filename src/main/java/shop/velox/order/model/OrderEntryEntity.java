package shop.velox.order.model;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.availability.api.dto.AvailabilityDto;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class OrderEntryEntity {

  private String articleId;

  private String articleName;

  private AvailabilityDto availability;

  private BigDecimal quantity;

  private BigDecimal unitPrice;

  private BigDecimal price;

  List<MetadataEntryEntity> metadata;

}
