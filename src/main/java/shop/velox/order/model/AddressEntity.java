package shop.velox.order.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class AddressEntity {

  private String id;

  private String firstName;

  private String lastName;

  private String company;

  private String address;

  private String addressAddition;

  private String city;

  private String zipCode;

  private String poBox;

  private String country;

}
