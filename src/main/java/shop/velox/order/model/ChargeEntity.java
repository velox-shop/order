package shop.velox.order.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class ChargeEntity {

  private String type;

  private BigDecimal amount;
}
