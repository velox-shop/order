package shop.velox.order.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class MetadataEntryEntity {

  private String key;

  private List<MetadataValueEntity> values;

}
