package shop.velox.order.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.PaymentStatus;
import shop.velox.order.api.dto.ShipmentStatus;


@Data
@Builder
@Jacksonized
@FieldNameConstants
public class OrderEntity {

  @Id
  private String id;

  private String code;

  private String userId;

  private LocalDateTime placedDateTime;

  @CreatedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime createdDateTime;

  @LastModifiedDate
  @EqualsAndHashCode.Exclude
  private LocalDateTime lastModifiedDateTime;

  @Version
  @EqualsAndHashCode.Exclude
  private Long version;

  private List<OrderEntryEntity> entries;

  private AddressEntity shippingAddress;

  private AddressEntity billingAddress;

  private List<ChargeEntity> charges;

  private BigDecimal totalPrice;

  private String currencyIsoCode;

  private String cartCode;

  private OrderStatus orderStatus;

  private PaymentStatus paymentStatus;

  private ShipmentStatus shipmentStatus;

  List<MetadataEntryEntity> metadata;

}
