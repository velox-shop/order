package shop.velox.order.dao;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import shop.velox.order.model.OrderEntity;

public interface OrderRepository extends MongoRepository<OrderEntity, String>,
    OrderRepositoryCustom {

  @Override
  <S extends OrderEntity> S save(S entity);

  @Override
  Optional<OrderEntity> findById(String id);

}
