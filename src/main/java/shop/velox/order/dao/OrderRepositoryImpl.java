package shop.velox.order.dao;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.OrderEntity;
import shop.velox.order.model.OrderEntity.Fields;

@Repository
public class OrderRepositoryImpl implements OrderRepositoryCustom {

  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public Page<OrderEntity> findOrders(String userId, OrderStatus filter, String cartCode,
      Pageable pageable) {
    Query query = new Query();

    if(isNotBlank(userId)){
      query.addCriteria(Criteria.where(Fields.userId).is(userId));
    }

    if (filter != null) {
      query.addCriteria(Criteria.where(Fields.orderStatus).is(filter));
    }
    if (isNotBlank(cartCode)) {
      query.addCriteria(Criteria.where(Fields.cartCode).is(cartCode));
    }

    long total = mongoTemplate.count(query, OrderEntity.class);
    query.with(pageable);

    List<OrderEntity> orders = mongoTemplate.find(query, OrderEntity.class);
    return new PageImpl<>(orders, pageable, total);
  }
}
