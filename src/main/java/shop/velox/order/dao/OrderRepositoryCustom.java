package shop.velox.order.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.OrderEntity;

public interface OrderRepositoryCustom {

  Page<OrderEntity> findOrders(@Nullable String userId, @Nullable OrderStatus filter,
      @Nullable String cartCode, Pageable pageable);

}
