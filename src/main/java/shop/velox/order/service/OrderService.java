package shop.velox.order.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;

public interface OrderService {

  // Security check was not forgotten, Creating an order is allowed for everyone
  @PreAuthorize("true")
  OrderDto createOrder(CreateOrderDto orderDto);


  @PostAuthorize("@orderAuthorizationEvaluator.canAccessOrder(authentication, returnObject)")
  Optional<OrderDto> getOrder(String orderId);


  @PreAuthorize("@orderAuthorizationEvaluator.canAccessAllUserOrders(authentication, #userId)")
  Page<OrderDto> getAllByUserId(@Param("userId") String userId, Pageable pageable,
      OrderStatus filter, String cartCode);


  @PreAuthorize("@orderAuthorizationEvaluator.canAccessAnyOrder(authentication)")
  Page<OrderDto> getAll(Pageable pageable, OrderStatus filter, String cartCode);


  @PreAuthorize("@orderAuthorizationEvaluator.canAccessOrderWithId(authentication, #orderId)")
  OrderDto updateOrder(@Param("orderId") String orderId, UpdateOrderDto order);

  @PreAuthorize("@orderAuthorizationEvaluator.canAccessOrderWithId(authentication, #orderId)")
  OrderDto updateStatus(String orderId, OrderStatus orderStatus, String orderHash);
}
