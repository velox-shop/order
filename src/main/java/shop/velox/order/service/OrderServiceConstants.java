package shop.velox.order.service;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class OrderServiceConstants {

  public static class Authorities {

    public static final String ORDER_ADMIN = "Admin_Order";
  }
}
