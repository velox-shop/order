package shop.velox.order.service.impl;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;
import shop.velox.order.converter.CreateOrderConverter;
import shop.velox.order.converter.OrderConverter;
import shop.velox.order.converter.UpdateOrderPopulator;
import shop.velox.order.dao.OrderRepository;
import shop.velox.order.model.OrderEntity;
import shop.velox.order.service.OrderService;

@Component
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

  private final OrderRepository orderRepository;
  private final OrderConverter orderConverter;
  private final CreateOrderConverter createOrderConverter;
  private final UpdateOrderPopulator updateOrderPopulator;

  @Override
  public OrderDto createOrder(CreateOrderDto createOrderDto) {
    OrderEntity orderToSave = createOrderConverter.convert(createOrderDto);
    return orderConverter.convertEntityToDto(orderRepository.save(orderToSave));
  }

  @Override
  public Optional<OrderDto> getOrder(String orderId) {
    return orderRepository.findById(orderId)
        .map(orderConverter::convertEntityToDto);
  }

  @Override
  public Page<OrderDto> getAllByUserId(String userId, Pageable pageable, OrderStatus filter,
      String cartCode) {
    return orderRepository.findOrders(userId, filter, cartCode, pageable)
        .map(orderConverter::convertEntityToDto);
  }

  @Override
  public Page<OrderDto> getAll(Pageable pageable, OrderStatus filter, String cartCode) {
    return orderRepository.findOrders(null, filter, cartCode, pageable)
        .map(orderConverter::convertEntityToDto);
  }

  @Override
  public OrderDto updateOrder(String orderId, UpdateOrderDto order) {

    OrderEntity oldOrderEntity = orderRepository.findById(orderId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "order with id: " + orderId + "does not exist"));

    var oldOrderDto = orderConverter.convertEntityToDto(oldOrderEntity);

    if (!StringUtils.equals(oldOrderDto.getHash(), order.getHash())) {
      log.debug("oldOrderDto: {}",
          ReflectionToStringBuilder.toString(oldOrderDto, ToStringStyle.JSON_STYLE));
      throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "hash does not match");
    }


    //TODO When payment and shipping are introduced, reimplement payment and shipping logic
    if ((oldOrderEntity.getPaymentStatus() != order.getPaymentStatus() || (
        oldOrderEntity.getShipmentStatus() != order.getShipmentStatus()))) {
        throw new ResponseStatusException(HttpStatus.CONFLICT, "changing payment/shipment status is not allowed");
    }

    updateOrderPopulator.update(order, oldOrderEntity);

    OrderDto updatedOrderDto = orderConverter.convertEntityToDto(
        orderRepository.save(oldOrderEntity));
    log.debug("Updated order: {}", updatedOrderDto);
    return updatedOrderDto;
  }

  @Override
  public OrderDto updateStatus(String orderId, OrderStatus orderStatus, String orderHash) {
    OrderEntity oldOrderEntity = orderRepository.findById(orderId)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "order with id: " + orderId + "does not exist"));

    var oldOrderDto = orderConverter.convertEntityToDto(oldOrderEntity);

    if (!StringUtils.equals(oldOrderDto.getHash(), orderHash)) {
      throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "hash does not match");
    }

    oldOrderEntity.setOrderStatus(orderStatus);
    return orderConverter.convertEntityToDto(orderRepository.save(oldOrderEntity));
  }
}
