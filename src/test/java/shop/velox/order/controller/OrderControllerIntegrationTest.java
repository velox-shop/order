package shop.velox.order.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.opentest4j.AssertionFailedError;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import shop.velox.order.AbstractWebIntegrationTest;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.MetadataEntryDto;
import shop.velox.order.api.dto.MetadataValueDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderEntryDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.api.dto.UpdateOrderDto;

@TestPropertySource(properties = {
    "logging.level.org.springframework.web.client=TRACE",
    "logging.level.shop.velox.order.api.client=DEBUG",
    "logging.level.shop.velox.order.permissions=DEBUG",
    "logging.level.shop.velox.commons.security.service=DEBUG",
    "logging.level.shop.velox.order.service.impl.OrderServiceImpl=DEBUG",
})
@TestMethodOrder(OrderAnnotation.class)
@Slf4j
public class OrderControllerIntegrationTest extends AbstractWebIntegrationTest {

  public static final String CART_CODE = "cartCode";
  private static String orderId;
  private static String orderHashWhenEmpty;
  private static String orderHashWhenOrdered;

  @Test
  @Order(1)
  void user1CreatesOrderTest() {
    CreateOrderDto createOrderDto = CreateOrderDto.builder()
        .orderStatus(OrderStatus.DRAFT)
        .cartCode(CART_CODE)
        .build();

    ResponseEntity<OrderDto> response;
    try {
      response = getUser1OrderApiClient().createOrder(createOrderDto);
    } catch (Exception e) {
      throw new AssertionFailedError("Failed to create order", e);
    }

    assertNotNull(response);
    assertEquals(CREATED, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    orderId = body.getId();
    assertThat(orderId).isNotBlank();
    assertThat(body.getUserId()).isNotBlank();
    assertThat(body.getUserId()).isEqualTo(LOCAL_USER_1_USERNAME);
  }

  @Test
  @Order(2)
  void user2CannotGetOrderTest() {
    assertThat(orderId).isNotBlank();
    assertThrows(HttpClientErrorException.Forbidden.class,
        () -> getUser2OrderApiClient().getOrder(orderId));
  }

  @Test
  @Order(2)
  void user1CannotGetNotExistingOrderTest() {
    assertThrows(HttpClientErrorException.NotFound.class,
        () -> getUser1OrderApiClient().getOrder("NotExistingId"));
  }

  @Test
  @Order(2)
  void user1CanGetOrderTest() {
    assertThat(orderId).isNotBlank();
    ResponseEntity<OrderDto> response;
    try {
      response = getUser1OrderApiClient().getOrder(orderId);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get order with Status: " + e.getStatusCode(), e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertEquals(orderId, body.getId());
    String receivedHash = response.getBody().getHash();
    assertThat(receivedHash).isNotBlank();
    orderHashWhenEmpty = receivedHash;
  }

  @Test
  @Order(4)
  void user2CannotUpdateOrderTest() {
    assertThat(orderId).isNotBlank();
    UpdateOrderDto updateOrderDto = UpdateOrderDto.builder()
        .cartCode(CART_CODE)
        .hash(orderHashWhenEmpty)
        .build();
    assertThrows(HttpClientErrorException.Forbidden.class,
        () -> getUser2OrderApiClient().updateOrder(orderId, updateOrderDto));
  }

  @Test
  @Order(4)
  void user1CannotUpdateNotExistingOrderTest() {
    assertThat(orderId).isNotBlank();
    UpdateOrderDto updateOrderDto = UpdateOrderDto.builder()
        .cartCode(CART_CODE)
        .hash(orderHashWhenEmpty)
        .build();
    assertThrows(HttpClientErrorException.NotFound.class,
        () -> getUser1OrderApiClient().updateOrder("NotExisting", updateOrderDto));
  }

  @Test
  @Order(4)
  void user1CannotUpdateOrderWithInvalidHashTest() {
    assertThat(orderId).isNotBlank();
    UpdateOrderDto updateOrderDto = UpdateOrderDto.builder()
        .cartCode(CART_CODE)
        .hash("invalidHash")
        .build();
    var exception = assertThrows(HttpClientErrorException.class,
        () -> getUser1OrderApiClient().updateOrder(orderId, updateOrderDto));
    assertEquals(HttpStatus.PRECONDITION_FAILED, exception.getStatusCode());
  }

  @Test
  @Order(4)
  void user1CanUpdateOrder() {
    log.info("user1CanUpdateOrder test started");
    assertThat(orderId).isNotBlank();
    LocalDateTime now = LocalDateTime.now(Clock.systemUTC()).withNano(0);
    UpdateOrderDto updateOrderDto = UpdateOrderDto.builder()
        .cartCode(CART_CODE)
        .entries(List.of(OrderEntryDto.builder()
            .articleId("articleId1")
            .quantity(BigDecimal.ONE)
            .metadata(List.of(MetadataEntryDto.builder()
                .key("key1_1")
                .values(List.of(MetadataValueDto.builder()
                    .stringValue("value1_1")
                    .build()))
                .build()))
            .build()))
        .orderStatus(OrderStatus.ORDERED)
        .placedDateTime(now)
        .hash(orderHashWhenEmpty)
        .build();
    var updateResponseEntity = getUser1OrderApiClient().updateOrder(orderId, updateOrderDto);

    assertNotNull(updateResponseEntity);
    assertEquals(OK, updateResponseEntity.getStatusCode());
    var updatedOrderDto = updateResponseEntity.getBody();
    assertNotNull(updatedOrderDto);
    assertEquals(orderId, updatedOrderDto.getId());
    assertEquals(OrderStatus.ORDERED, updatedOrderDto.getOrderStatus());
    assertEquals(now, updatedOrderDto.getPlacedDateTime());
    assertNotNull(updatedOrderDto.getCreatedDateTime());
    LocalDateTime lastModifiedDateTime = updatedOrderDto.getLastModifiedDateTime();
    assertNotNull(lastModifiedDateTime);
    log.info("LastModifiedDateTime: {}", lastModifiedDateTime);
    var hash = updatedOrderDto.getHash();
    assertNotEquals(orderHashWhenEmpty, hash);
    orderHashWhenOrdered = hash;
  }

  @Test
  @Order(1)
  void user2CreatesOrderTest() {
    CreateOrderDto createOrderDto = CreateOrderDto.builder()
        .orderStatus(OrderStatus.DRAFT)
        .cartCode("cartCode2")
        .metadata(List.of(MetadataEntryDto.builder()
            .key("key1")
            .values(List.of(MetadataValueDto.builder()
                .stringValue("value1")
                .build()))
            .build()))
        .entries(List.of(OrderEntryDto.builder()
            .articleId("articleId1")
            .quantity(BigDecimal.ONE)
            .metadata(List.of(MetadataEntryDto.builder()
                .key("key1_1")
                .values(List.of(MetadataValueDto.builder()
                    .stringValue("value1_1")
                    .build()))
                .build()))
            .build()))
        .build();

    ResponseEntity<OrderDto> response;
    try {
      response = getUser2OrderApiClient().createOrder(createOrderDto);
    } catch (Exception e) {
      throw new AssertionFailedError("Failed to create order", e);
    }

    assertNotNull(response);
    assertEquals(CREATED, response.getStatusCode());
    var createdOrder = response.getBody();
    assertNotNull(createdOrder);
    var orderId = createdOrder.getId();
    assertThat(orderId).isNotBlank();
    assertThat(createdOrder.getUserId()).isNotBlank();
    assertThat(createdOrder.getUserId()).isEqualTo(LOCAL_USER_2_USERNAME);
    assertThat(createdOrder.getMetadata()).isNotEmpty();
    assertThat(createdOrder.getMetadata().size()).isEqualTo(1);
    assertThat(createdOrder.getMetadata().getFirst().getKey()).isEqualTo("key1");
    assertThat(createdOrder.getMetadata().getFirst().getValues()).isNotEmpty();
    assertThat(createdOrder.getMetadata().getFirst().getValues().size()).isEqualTo(1);
    assertThat(createdOrder.getMetadata().getFirst().getValues().getFirst().getStringValue())
        .isEqualTo("value1");
    assertThat(createdOrder.getEntries()).isNotEmpty();
    assertThat(createdOrder.getEntries().size()).isEqualTo(1);
    assertThat(createdOrder.getEntries().getFirst().getArticleId()).isEqualTo("articleId1");
    assertThat(createdOrder.getEntries().getFirst().getQuantity()).isEqualTo(BigDecimal.ONE);
    assertThat(createdOrder.getEntries().getFirst().getMetadata()).isNotEmpty();
    assertThat(createdOrder.getEntries().getFirst().getMetadata().size()).isEqualTo(1);
    assertThat(createdOrder.getEntries().getFirst().getMetadata().getFirst().getKey()).isEqualTo(
        "key1_1");
    assertThat(
        createdOrder.getEntries().getFirst().getMetadata().getFirst().getValues()).isNotEmpty();
    assertThat(
        createdOrder.getEntries().getFirst().getMetadata().getFirst().getValues().size()).isEqualTo(
        1);
    assertThat(createdOrder.getEntries().getFirst().getMetadata().getFirst().getValues().getFirst()
        .getStringValue())
        .isEqualTo("value1_1");
  }

  @Test
  @Order(5)
  void userCannotGetAllOrdersTest() {
    assertThrows(HttpClientErrorException.Forbidden.class,
        () -> getUser1OrderApiClient().getAllOrders(Pageable.ofSize(1), null, null, null));
  }

  @Test
  @Order(5)
  void adminGetsAllOrdersTest() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getAdminOrderApiClient().getAllOrders(Pageable.ofSize(1), null, null, null);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertThat(body.getContent()).isNotEmpty();
    assertEquals(2, body.getTotalElements());
  }

  @Test
  @Order(5)
  void adminGetsAllOrdersWithStatusTest() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getAdminOrderApiClient().getAllOrders(Pageable.ofSize(1), null,
          OrderStatus.ORDERED, null);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertThat(body.getContent()).isNotEmpty();
    assertEquals(1, body.getTotalElements());
    OrderDto order = body.getContent().getFirst();
    assertEquals(OrderStatus.ORDERED, order.getOrderStatus());
  }

  @Test
  @Order(5)
  void user2CannotGetAllOrdersOfUser1Test() {
    assertThrows(HttpClientErrorException.Forbidden.class,
        () -> getUser2OrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
            Pageable.ofSize(1), null, null));
  }

  @Test
  @Order(5)
  void anonymousCannotGetAllOrdersOfUser1Test() {
    assertThrows(HttpClientErrorException.Forbidden.class,
        () -> getAnonymousOrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
            Pageable.ofSize(1), null, null));
  }

  @Test
  @Order(5)
  void user1GetsOwnOrders() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getUser1OrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
          Pageable.ofSize(1), null, null);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertThat(body.getContent()).isNotEmpty();
    assertEquals(1, body.getTotalElements());
    OrderDto order = body.getContent().getFirst();
    assertEquals(LOCAL_USER_1_USERNAME, order.getUserId());
  }

  @Test
  @Order(5)
  void user1GetsOwnOrdersWithStatusDraft() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getUser1OrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
          Pageable.ofSize(1), OrderStatus.DRAFT, null);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertEquals(0, body.getTotalElements());
  }

  @Test
  @Order(5)
  void user1GetsOwnOrdersWithStatusOrdered() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getUser1OrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
          Pageable.ofSize(1), OrderStatus.ORDERED, null);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertThat(body.getContent()).isNotEmpty();
    assertEquals(1, body.getTotalElements());
    OrderDto order = body.getContent().getFirst();
    assertEquals(LOCAL_USER_1_USERNAME, order.getUserId());
    assertEquals(OrderStatus.ORDERED, order.getOrderStatus());
  }

  @Test
  @Order(6)
  void user1GetsOwnOrderWithStatusAndCartCodeFilters() {
    ResponseEntity<? extends Page<OrderDto>> response;
    try {
      response = getUser1OrderApiClient().getAllOrdersFromUser(LOCAL_USER_1_USERNAME,
          Pageable.ofSize(1), OrderStatus.ORDERED, CART_CODE);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError("Failed to get all orders with Status: " + e.getStatusCode(),
          e);
    }

    assertNotNull(response);
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertThat(body.getContent()).isNotEmpty();
    assertEquals(1, body.getTotalElements());
    OrderDto order = body.getContent().getFirst();
    assertEquals(LOCAL_USER_1_USERNAME, order.getUserId());
    assertEquals(OrderStatus.ORDERED, order.getOrderStatus());
    assertEquals(CART_CODE, order.getCartCode());
  }

  @Test
  @Order(7)
  void user1CanUpdateOrderStatus() {
    assertThat(orderId).isNotBlank();
    assertThat(orderHashWhenOrdered).isNotBlank();

    ResponseEntity<OrderDto> response;
    try {
      response = getUser1OrderApiClient().updateOrderStatus(orderId, OrderStatus.EXPORTED,
          orderHashWhenOrdered);
    } catch (HttpStatusCodeException e) {
      throw new AssertionFailedError(
          "Failed to update order status with HTTP Status: " + e.getStatusCode(),
          e);
    }
    assertEquals(OK, response.getStatusCode());
    var body = response.getBody();
    assertNotNull(body);
    assertEquals(orderId, body.getId());
    assertEquals(OrderStatus.EXPORTED, body.getOrderStatus());
  }


}
