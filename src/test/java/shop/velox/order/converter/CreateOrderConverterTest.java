package shop.velox.order.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import shop.velox.order.AbstractIntegrationTest;
import shop.velox.order.api.dto.AddressDto;
import shop.velox.order.api.dto.CreateOrderDto;
import shop.velox.order.api.dto.MetadataEntryDto;
import shop.velox.order.api.dto.MetadataValueDto;
import shop.velox.order.api.dto.OrderEntryDto;
import shop.velox.order.api.dto.OrderStatus;
import shop.velox.order.model.AddressEntity;
import shop.velox.order.model.MetadataEntryEntity;
import shop.velox.order.model.OrderEntity;

public class CreateOrderConverterTest extends AbstractIntegrationTest {

  @Resource
  private CreateOrderConverter createOrderConverter;

  @Test
  void convertTest(){
    CreateOrderDto createOrderDto = CreateOrderDto.builder()
        .orderStatus(OrderStatus.DRAFT)
        .cartCode("cartCode2")
        .metadata(List.of(MetadataEntryDto.builder()
            .key("key1")
            .values(List.of(MetadataValueDto.builder()
                .stringValue("value1")
                .build()))
            .build()))
        .entries(List.of(OrderEntryDto.builder()
            .articleId("articleId1")
            .quantity(BigDecimal.ONE)
            .metadata(List.of(MetadataEntryDto.builder()
                .key("key1_1")
                .values(List.of(MetadataValueDto.builder()
                    .stringValue("value1_1")
                    .build()))
                .build()))
            .build()))
        .billingAddress(AddressDto.builder()
            .addressAddition("addressAddition1")
            .build())
        .build();

    OrderEntity createdOrder = createOrderConverter.convert(createOrderDto);

    assertNotNull(createdOrder);
    assertThat(createdOrder.getCartCode()).isEqualTo("cartCode2");
    assertThat(createdOrder.getOrderStatus()).isEqualTo(OrderStatus.DRAFT);
    
    assertThat(createdOrder.getMetadata()).isNotEmpty();
    assertThat(createdOrder.getMetadata().size()).isEqualTo(1);
    MetadataEntryEntity orderFirstMetadataEntry = createdOrder.getMetadata().getFirst();
    assertThat(orderFirstMetadataEntry.getKey()).isEqualTo("key1");
    assertThat(orderFirstMetadataEntry.getValues()).isNotEmpty();
    assertThat(orderFirstMetadataEntry.getValues().size()).isEqualTo(1);
    assertThat(orderFirstMetadataEntry.getValues().getFirst().getStringValue())
        .isEqualTo("value1");

    assertThat(createdOrder.getEntries()).isNotEmpty();
    assertThat(createdOrder.getEntries().size()).isEqualTo(1);
    assertThat(createdOrder.getEntries().getFirst().getArticleId()).isEqualTo("articleId1");
    assertThat(createdOrder.getEntries().getFirst().getQuantity()).isEqualTo(BigDecimal.ONE);
    List<MetadataEntryEntity> firstEntryFirstMetadataEntry = createdOrder.getEntries().getFirst()
        .getMetadata();
    assertThat(firstEntryFirstMetadataEntry).isNotEmpty();
    assertThat(firstEntryFirstMetadataEntry.size()).isEqualTo(1);
    assertThat(firstEntryFirstMetadataEntry.getFirst().getKey()).isEqualTo("key1_1");
    assertThat(firstEntryFirstMetadataEntry.getFirst().getValues()).isNotEmpty();
    assertThat(firstEntryFirstMetadataEntry.getFirst().getValues().size()).isEqualTo(1);
    assertThat(
        firstEntryFirstMetadataEntry.getFirst().getValues().getFirst().getStringValue()).isEqualTo(
        "value1_1");

    AddressEntity billingAddress = createdOrder.getBillingAddress();
    assertThat(billingAddress).isNotNull();
    assertThat(billingAddress.getAddressAddition()).isEqualTo("addressAddition1");

  }

}
