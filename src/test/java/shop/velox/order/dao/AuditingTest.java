package shop.velox.order.dao;

import static org.assertj.core.api.Assertions.assertThat;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import shop.velox.order.AbstractIntegrationTest;
import shop.velox.order.model.OrderEntity;

public class AuditingTest extends AbstractIntegrationTest {

  @Resource
  private OrderRepository orderRepository;

  @Test
  void test() {
    var order = orderRepository.save(OrderEntity.builder()
            .code("code")
        .build());

    assertThat(order.getCreatedDateTime()).isNotNull();
    assertThat(order.getLastModifiedDateTime()).isNotNull();
    assertThat(order.getVersion()).isNotNull();
  }

}
