package shop.velox.order;

import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;

import jakarta.annotation.Resource;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import shop.velox.order.api.client.OrderApiClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  private final Logger log = LoggerFactory.getLogger(AbstractWebIntegrationTest.class);

  public static final String HOST_URL = "http://localhost:%s";
  
  @Resource
  protected RestTemplateBuilder restTemplateBuilder;

  @Value(value = "${local.server.port}")
  protected int port;

  @Getter(AccessLevel.PROTECTED)
  private OrderApiClient anonymousOrderApiClient;

  @Getter(AccessLevel.PROTECTED)
  private OrderApiClient user1OrderApiClient;

  @Getter(AccessLevel.PROTECTED)
  private OrderApiClient user2OrderApiClient;

  @Getter(AccessLevel.PROTECTED)
  private OrderApiClient adminOrderApiClient;

  @BeforeEach
  public void setup() throws InterruptedException {
    log.info("Web test method setup");

    anonymousOrderApiClient = new OrderApiClient(
        restTemplateBuilder.build(), String.format(HOST_URL, port));

    user1OrderApiClient = new OrderApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_1_USERNAME, LOCAL_USER_1_PASSWORD)
            .build(), String.format(HOST_URL, port));

    user2OrderApiClient = new OrderApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_2_USERNAME, LOCAL_USER_2_PASSWORD)
            .build(), String.format(HOST_URL, port));

    adminOrderApiClient = new OrderApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD)
            .build(), String.format(HOST_URL, port));
  }

}
