# Work locally
Build Service

```
./gradlew clean build 
```

Execute with Gradle
```
./gradlew bootRun
```

Execute with Docker Compose
```
docker-compose stop && docker-compose rm -fv && docker-compose up --build --force-recreate --remove-orphans
```

Test
```
newman run src/test/*postman_collection.json --environment src/test/veloxOrder-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation
Online API Documentation is at http://localhost:8449/order/v1/swagger-ui.html

Offline API Documentation is at https://velox-shop.gitlab.io/order/swagger/

